class Fixnum

def in_words
one_to_nine = {0 => 'zero', 1 => 'one', 2 => 'two', 3 => 'three', 4 => 'four', 5 => 'five',
                      6 => 'six', 7 => 'seven', 8 => 'eight', 9 => 'nine'}
teens = {10 => 'ten', 11 => 'eleven', 12 => 'twelve', 13 => 'thirteen',
             14 => 'fourteen', 15 => 'fifteen', 16 => 'sixteen', 17 => 'seventeen',
             18 => 'eighteen', 19 => 'nineteen'}

tens =   {20 => 'twenty', 30 => 'thirty', 40 => 'forty',
               50 => 'fifty', 60 => 'sixty', 70 => 'seventy', 80 => 'eighty',
               90 => 'ninety'}
magnitude = {100 => 'hundred', 1000 => 'thousand', 1000000 => 'million',
             10000000 => 'billion', 100000000 => 'trillion'}

if self < 10
  one_to_nine[self]
elsif self < 20
  teens[self]
elsif self < 100
  if self%10 == 0
    tens[self]
  else
    tens,ones = self.divmod(10)
    tens *= 10 
    tens[ten] + ' ' + one_to_nine[ones]
  end
else
  mag = self.find_mag
  q,r = self.divmod(mag)
  if r == 0
    one_to_nine[q] + ' ' + magnitude[mag]
  else
    q.in_words + " " + magnitude[mag] + " " + r.in_words
  end
end



end

def find_mag
  magnitude = {100 => 'hundred', 1000 => 'thousand', 1000000 => 'million',
                 10000000 => 'billion', 100000000 => 'trillion'}

  magnitude.keys.take_while {|mag| mag <= self}.last
end


end
